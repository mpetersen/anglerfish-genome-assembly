rule fastp_pe:
	input:
		sample = [ "data/raw/{sample}_R1.merged.fastq.gz", "data/raw/{sample}_R2.merged.fastq.gz" ]
	output:
		trimmed = ["data/clean/{sample}_R1.merged.fastq.gz", "data/clean/{sample}_R2.merged.fastq.gz"],
		html = "report/fastp/{sample}.html",
		json = "report/fastp/{sample}.json"
	log:
		"logs/fastp/{sample}.log"
	params:
		extra = ""
	threads: 24
	wrapper:
		"0.51.3/bio/fastp"


# customised from snakemake wrapper here:
# https://snakemake-wrappers.readthedocs.io/en/stable/wrappers/fastqc.html
rule fastqc:
	input:
		"data/clean/{sample}_R{group}.merged.fastq.gz"
	output:
		html = "report/fastqc/{sample}_R{group}.merged_fastqc.html",
		zip  = "report/fastqc/{sample}_R{group}.merged_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file
	conda:
		"../envs/fastqc.yaml"
	threads:
		2
	params: "--nogroup"
	log:
		"logs/fastqc/{sample}_R{group}.log"
	shell:
		"""
		fastqc --threads {threads} {params} --outdir $(dirname {output.html}) {input} > {log}
		"""

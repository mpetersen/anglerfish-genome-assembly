rule bwa_mem:
    input:
        reads=["data/clean/{sample}_R1.merged.fastq.gz", "data/clean/{sample}_R2.merged.fastq.gz"]
    output:
        "results/2020-05-05_bwa-mem/{sample}.bam"
    log:
        "logs/bwa_mem/{sample}.log"
    params:
        index="results/assembly/male/spades/contigs.fasta",
        extra=r"-R '@RG\tID:{sample}\tSM:{sample}'",
        sort="none",             # Can be 'none', 'samtools' or 'picard'.
        sort_order="queryname",  # Can be 'queryname' or 'coordinate'.
        sort_extra=""            # Extra args for samtools/picard.
    threads: 24
    wrapper:
        "0.53.0/bio/bwa/mem"

rule insert_size_metrics:
    input:
        "results/2020-05-05_bwa-mem/{sample}.bam"
    output:
        metrics = "results/2020-05-05_bwa-mem/{sample}.insert_size_metrics.txt",
        histogram =  "results/2020-05-05_bwa-mem/{sample}.insert_size_histogram.pdf"
    params:
        "M=0.05"
    envmodules:
        "picard-tools"
    shell:
        """
        java -jar /package/picard-tools-2.9.2/picard.jar CollectInsertSizeMetrics \
        I={input} \
        O={output.metrics} \
        H={output.histogram} \
        {params}
        """


rule purge_haplotigs:
    input:
        "results/assembly/male/spades/contigs.fasta"
    output:
        "results/assembly/male/purge_haplotigs/contigs.curated.fasta"
    conda:
        "../envs/purge_haplotigs.yaml"
    params:
        ""
    shell:
        """
        purge_haplotigs hist {input}
        """

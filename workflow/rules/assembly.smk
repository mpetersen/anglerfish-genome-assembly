rule spades:
	input:
		R1 = "data/clean/{sample}_R1.merged.fastq.gz",
		R2 = "data/clean/{sample}_R2.merged.fastq.gz"
	output:
		"results/assembly/{sample}/spades"
	log:
		"logs/spades/{sample}.spades.log"
	envmodules:
		"spades"
	conda:
		"../envs/spades.yaml"
	params:
		""
	threads:
		48
	shell:
		"""
		export TMPDIR=/scratch/local
		tempdir=$(mktemp -d)
		spades.py -t {threads} {params} --tmp-dir $tempdir -1 {input.R1} -2 {input.R2} -o {output} > {log}
		rm -rf $tempdir
		"""

from snakemake.utils import validate
import pandas as pd

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
#singularity: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

units = pd.read_csv(config["units"], sep="\t").set_index("sample", drop=False)
units.index.names = ["sample_id"]
validate(units, schema="../schemas/units.schema.yaml")

def is_single_end(sample, unit):
	"""Determine whether unit is single-end."""
	return pd.isnull(units.loc[(sample, unit), "fq2"])

def get_fastqs(wildcards):
	"""Get raw FASTQ files from unit sheet."""
	if is_single_end(wildcards.sample, wildcards.unit):
		return units.loc[ (wildcards.sample, wildcards.unit), "fq1" ]
	else:
		u = units.loc[ (wildcards.sample), ["fq1", "fq2"] ].dropna()
		return [ f"{u.fq1}", f"{u.fq2}" ]


rule mergeFQ:
	input:
		R1 = lambda wildcards: units.loc[ wildcards.sample, "fq1" ],
		R2 = lambda wildcards: units.loc[ wildcards.sample, "fq2" ]
	output:
		R1 = temp("data/raw/{sample}_R1.merged.fastq.gz"),
		R2 = temp("data/raw/{sample}_R2.merged.fastq.gz")
	shell:
		"""
		cat {input.R1} > {output.R1}
		cat {input.R2} > {output.R2}
		"""

rule raw_data_stats:
	input:
		expand("data/raw/{sample}_R{group}.merged.fastq.gz", sample = units["sample"], group = [1, 2])
	output:
		"report/raw-data.stats"
	conda:
		"../envs/seqkit.yaml"
	params:
		"-b"
	shell:
		"seqkit stats {params} {input} > {output}"


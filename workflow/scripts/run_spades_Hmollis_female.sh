#!/bin/bash

set -euo pipefail

module load spades

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/Haplophryne/Haplophryne_mollis/spades"

mkdir -p $outdir logs/spades

# 64 threads, 1000 GB RAM
spades.py --threads 64 --memory 1000 --tmp-dir $tempdir --dataset config/dataset_Hmollis_female.yaml -o $outdir > logs/spades/Hmollis_female.spades.log

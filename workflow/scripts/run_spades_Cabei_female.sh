#!/bin/bash

set -euo pipefail

module load spades

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/Chaunax/Chaunax_abei/spades"

mkdir -p $outdir logs/spades

# 64 threads, 420 GB RAM
spades.py --threads 64 --memory 420 --tmp-dir $tempdir --dataset config/dataset_Cabei_female.yaml -o $outdir >> logs/spades/Cabei_female.spades.log

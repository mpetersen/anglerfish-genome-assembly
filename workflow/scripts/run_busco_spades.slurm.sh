#!/bin/bash
#SBATCH --ntasks-per-node=1
#SBATCH -D /data/processing5/petersen/projects/anglerfish-genome-assembly
#SBATCH -c 12
#SBATCH -J busco.Cabei
#SBATCH --mem-per-cpu=5781
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o /data/processing5/petersen/projects/anglerfish-genome-assembly/busco.Cabei.%j.out
#SBATCH -e /data/processing5/petersen/projects/anglerfish-genome-assembly/busco.Cabei.%j.err

TMPDIR=/scratch/local//
scratch=$(mktemp -p $TMPDIR -d -t tmp.XXXXXXXXXX)

export TMPDIR=$scratch/
export TMP=$scratch/
export TEMP=$scratch/
export LC_ALL=en_US.UTF-8; export LANG=en_US.UTF-8; export LANGUAGE=en_US.UTF-8;
function cleanup {
cd /
    rm -rf $scratch
}
trap cleanup SIGTERM SIGKILL SIGUSR2


export AUGUSTUS_CONFIG_PATH="code/conda/config"
srun -K busco --cpu 12 --mode genome -i results/assembly/Chaunax/Chaunax_abei/spades/scaffolds.fasta -o busco_Cabei --out_path results/busco --lineage actinopterygii
cleanup

#!/bin/bash

set -euo pipefail

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/male/spades"

mkdir -p $outdir

spades.py --threads 92 --memory 1500 --tmp-dir $tempdir --dataset config/dataset_male.yaml -o $outdir > logs/spades/male.spades.log

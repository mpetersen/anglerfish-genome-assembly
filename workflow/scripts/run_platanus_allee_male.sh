#!/bin/bash

set -euo pipefail

# Platanus-allee requires minimap2
module load minimap2

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/male/platanus_allee"

threads=92
max_mem=1500

echo "## Creating output directory $outdir"
mkdir -p $outdir

echo "## Linking input files to $outdir"
for file in data/clean/*.merged.fastq.gz; do
    ln -rsf $file $outdir/
done
echo "## Linking platanus_allee binary to $outdir"
ln -rs ./resources/Platanus_allee_v2.2.2_Linux_x86_64/platanus_allee $outdir/

echo '## Stage I: assemble'
cd $outdir

./platanus_allee assemble -t $threads -m $max_mem -tmp "$tempdir" -K 0.95 -s 32 -c 15 -o Pspin_male -f *.fastq.gz > platanus_allee.log

echo '## Stage II: phase'

echo '## Stage III: consensus'

echo '## Done. Removing tempdir'
rm -r "$tempdir"

echo '## All done. Bye.'

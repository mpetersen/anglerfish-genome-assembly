#!/bin/bash

set -euo pipefail

module load spades

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/Cryptopsaras/Cryptopsaras_couesii/spades"

mkdir -p $outdir logs/spades

# 64 threads, 1000 GB RAM -- this one needs more
spades.py --threads 64 --memory 1500 --tmp-dir $tempdir --dataset config/dataset_Ccoue_female.yaml -o $outdir > logs/spades/Ccoue_female.spades.log

#!/bin/bash
#SBATCH --ntasks-per-node=1
#SBATCH -D /data/processing/petersen/projects/anglerfish-genome-assembly
#SBATCH -c 92
#SBATCH -J Pspin.m.spades
#SBATCH --nodelist=deep17
#SBATCH --mem-per-cpu=15000
#SBATCH --mail-user=petersen@ie-freiburg.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p bioinfo
#SBATCH -o /data/processing/petersen/projects/anglerfish-genome-assembly/Pspin.m.spades.20200430.%j.out
#SBATCH -e /data/processing/petersen/projects/anglerfish-genome-assembly/Pspin.m.spades.20200430.%j.err

TMPDIR=/scratch/local
scratch=$(mktemp -p $TMPDIR -d -t tmp.XXXXXXXXXX)

export TMPDIR=$scratch/
export TMP=$scratch/
export TEMP=$scratch/
export LC_ALL=en_US.UTF-8; export LANG=en_US.UTF-8; export LANGUAGE=en_US.UTF-8

function cleanup {
		cd /
    rm -rf $scratch
}

trap cleanup SIGTERM SIGKILL SIGUSR2

##
## Here begins the actual work
##

set -euo pipefail

module load spades

export TMPDIR=/scratch/local
tempdir=$(mktemp -d)
outdir="results/assembly/male/spades"

mkdir -p $outdir

srun -K spades.py --threads 92 --memory 1500 --tmp-dir $scratch --dataset config/dataset_male.yaml -o $outdir > logs/spades/male.spades.log

cleanup

# Anglerfish genome assembly

We have high-coverage, but short-insert Illumina data from anglerfish
samples of sometimes crappy quality. I used SPAdes to creade highly
fragmented, but still usable assemblies that form the basis for
immunogenetic analyses.

These species are covered as of 2022-03-03:

- *Chaunax abei*
- *Cryptopsaras couesii*
- *Gigantactis vanhoeffeni*
- *Haplophryne mollis*
- *Photocorynus spiniceps*

Most runscripts are in workflow/scripts. Have a look. It's all
hardcoded.
